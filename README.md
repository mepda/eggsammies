# Create Egg Sammie App with Vanilla Amplify cooked in

To set this up, you'll need the amplify cli installed, nodejs set up, and an AWS account.

Run ```npm i``` ```amplify init``` ```amplify add api```

When configuring the ```amplify add api``` step, say you want to make the route authenticated.

Properly set up your table using PK as the primary key and SK as the sort key. This will allow us to put all the necessary data in this one data store.

Fun thing I learned: While PKs have to be unique, once a SK is added, the PK is actually a combination of the two keys. That means you can query just the PK (while omitting the SK) at O(1)! Noice!

example of deployed app: https://d3c2df9qhqze2r.cloudfront.net/


Setup:

//using node > 14
npm i -g @aws-amplify/cli
npm i
amplfy init
amplify add api

- REST
- eggsammiesAPI
- /reviews
- eggsammieLambda
- NodeJS
- CRUD function for DynamoDB 
- Crete a new DynamoDB Table
- eggsammieTable
- eggsammieTable
- PK / string
- SK / string
(choose PK for primary key, and choose "yes" for wanting a sort key (auto chooses SK))
- no for advanced settings / edit local lambda
- authenticated users + guest users
- CRUD for authenticated users / Read for guests
- no to more paths

amplify add hosting
- Amazon CloudFront + S3
- PROD 
- rando name for S3 host bucket

amplify publish