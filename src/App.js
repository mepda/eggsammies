import './App.css';
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";

import { withAuthenticator } from '@aws-amplify/ui-react';
import '@aws-amplify/ui-react/styles.css';

import NavigationBar from './components/NavigationBar';
import SammiesForm from './components/SammiesForm';
import SammiesList from './components/SammiesList';
import Home from './components/Home';

function App({ signOut, user }) {
  return (
    <Router>
      <div className="App">
        <NavigationBar signOut={signOut} user={user} />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/add_review" element={<SammiesForm />} />
          <Route path="/see_reviews" element={<SammiesList />} />
        </Routes>
      </div>
    </Router>
  );
}

export default withAuthenticator(App);
