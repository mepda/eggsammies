import React from 'react'
import { Navbar, Container, Nav, Image, Button } from 'react-bootstrap'
import gudetama from "../imgs/gudetama.png"
import {
  Link
} from "react-router-dom";

function NavigationBar({ ...props }) {

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="#home">
          <Image src={gudetama} style={{ maxWidth: 75 }}></Image>
          <br />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/add_review">Add Review</Nav.Link>
            <Nav.Link as={Link} to="/see_reviews">See Reviews</Nav.Link>
          </Nav>
          <Nav className="justify-content-end" id="nav_end">
            <Navbar.Text>
              {props.user.username && <>Welcome: <u>{props.user.username}</u></>}
            </Navbar.Text>
            <Navbar.Text>
              <Button onClick={props.signOut}>Sign Out</Button>
            </Navbar.Text>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default NavigationBar