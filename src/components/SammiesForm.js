import React, { useState, useEffect } from 'react'
import { API, Auth } from 'aws-amplify'

import { Form, Container, Button, Image, Row, Col } from 'react-bootstrap'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEgg } from "@fortawesome/free-solid-svg-icons";
import gudetama_review from '../imgs/gudetama_review.png'

function SammiesForm() {
  const myAPI = "eggsammieAPI"
  const path = '/reviews';

  const [eggs, setEggs] = useState(3)
  const [title, setTitle] = useState('')
  const [details, setDetails] = useState('')


  const hyphenateName = (name) => {
    return name.split(" ").join("-")
  }

  const submitHandler = async (e) => {
    e.preventDefault()

    if (title.length == 0 && details.length == 0) {
      alert('Enter title + details before proceeding pluz')
      return
    }

    const hashedName = hyphenateName(title)
    const user = await Auth.currentAuthenticatedUser();
    const userId = user.pool.clientId;
    const userName = user.username;
    const created = new Date().toISOString();

    const review_data = {
      body: {
        PK: `REVIEW$${hashedName}`,
        SK: created,
        title,
        userId,
        userName,
        created,
        details,
        eggs
      }
    }

    const restaurant_data = {
      body: {
        PK: `RESTAURANTS`,
        SK: hashedName,
        name: title
      }
    }

    try {
      const response = await API.get(myAPI, `${path}/RESTAURANTS`)
      const check_exists = await API.get(myAPI, `${path}/REVIEW$${hashedName}`)
      if (check_exists.length == 0) {
        restaurant_data['body']['votes'] = 1;
        restaurant_data['body']['cummulative_ratings'] = Number(eggs);
      } else {
        response.filter((res) => res['SK'] == hashedName)
        let votes = Number(response[0]['votes'])
        let rating = Number(response[0]['cummulative_ratings'])
        restaurant_data['body']['votes'] = votes + 1;
        restaurant_data['body']['cummulative_ratings'] = rating + Number(eggs);
      }

      await API.post(myAPI, path, restaurant_data)
      await API.post(myAPI, path, review_data)
    } catch (error) {
      console.log(error)
    }

    setEggs(3)
    setTitle('')
    setDetails('')
  }

  return (
    <Container className="container_primary p-5">
      <Row>
        <Col>
          <Form onSubmit={submitHandler} style={{ position: 'relative' }}>
            <Form.Group className="m-3" controlId="exampleForm.ControlInput1">
              <h4 style={{ float: 'left' }}>Saucy Restaurant</h4>
              <Form.Control type="text" placeholder={"nommers house of nomming"} value={title} onChange={(e) => setTitle(e.target.value)} />
            </Form.Group>
            <Form.Group className="m-3" controlId="exampleForm.ControlTextarea1">
              <h4 style={{ float: 'left' }}>Crispy Details</h4>
              <Form.Control as="textarea" placeholder="what was good? what'd you recommend? anything not so hot?" rows={3}
                value={details} onChange={(e) => setDetails(e.target.value)} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
              <div style={{ display: 'flex', justifyContent: 'space-evenly', alignItems: 'center', height: '80px' }}>
                <h4>Egg Sammie Rating</h4>
                <div style={{}}>
                  <input style={{ background: '#fcb040' }} type="range" min="0" max="5" value={eggs} onChange={(e) => { setEggs(e.target.value) }} />
                  {eggs} x
                </div>
                <FontAwesomeIcon className={eggs == 5 ? 'spinny_if_big' : ''} style={{ position: 'absolute', right: -20, top: -40, color: '#fcb040' }} icon={faEgg} size={eggs + 'x'} />
              </div>
            </Form.Group>
            <Button className="btn" style={{ width: '95%', backgroundColor: '#fcb040' }} type="submit">Cook Up Review</Button>
          </Form>
        </Col>
        <Col>
          <h2 className="hide_if_mobile">Cook up a Sammie Review</h2>
          <Image src={gudetama_review} style={{ maxWidth: 250 }} />
        </Col>
      </Row>
    </Container >
  )
}

export default SammiesForm