import { useState, useEffect } from 'react'
import { Container, ListGroup, Button, Offcanvas } from 'react-bootstrap'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEgg } from "@fortawesome/free-solid-svg-icons";
import { API } from 'aws-amplify'



function OffCanvas({ props }) {

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [reviews, setReviews] = useState([])
  const myAPI = "eggsammieAPI"
  const path = '/reviews';

  useEffect(async () => {
    try {
      const shops = await API.get(myAPI, `${path}/REVIEW$${props.SK}`)
      setReviews(shops)
    } catch (error) {
      console.log(error)
    }
  }, [])

  return (
    <div>
      <Button variant="primary" style={{ borderRadius: '3px', outline: '3px solid white' }} onClick={handleShow} className="me-2">
        ?
      </Button>
      <Offcanvas show={show} onHide={handleClose} {...props}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>{props.name}</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          {reviews.map((review, i) => (
            <div key={i}>
              <h3>
                {review.userName}
              </h3>
              <p>
                {review.details}
              </p>
              <p>
                {review.eggs}
              </p>
            </div>
          ))}
        </Offcanvas.Body>
      </Offcanvas>
    </div>
  )
}





const RenderSammieShop = ({ props }) => {
  const rating = Math.ceil(Number(props.cummulative_ratings) / Number(props.votes))
  return (
    <ListGroup.Item
      key={props.SK}
      className="align_data"
      href={'#' + props.SK.split(" ").join("-")}>

      <span>{props.name}</span>
      <OffCanvas props={props} placement='bottom' name='bottom' />
      <div style={{ minWidth: 70, textAlign: 'left' }}>
        {[...Array(Number(rating))].map((el, i) => (
          <FontAwesomeIcon key={i} icon={faEgg} />
        ))}
      </div>
    </ListGroup.Item>
  )
}

function SammiesList() {
  const [sammieShops, setSammieShops] = useState([])

  const myAPI = "eggsammieAPI"
  const path = '/reviews';

  useEffect(async () => {
    try {
      const shops = await API.get(myAPI, `${path}/RESTAURANTS`)
      setSammieShops(shops)
    } catch (error) {
      console.log(error)
    }
  }, [])

  return (
    <Container className="container_primary mt-5">
      <ListGroup>
        {sammieShops.map((el, i) => {
          return <RenderSammieShop props={el} key={i} />
        })}
      </ListGroup>
    </Container >
  )
}

export default SammiesList