import React from 'react'
import { Image } from 'react-bootstrap'
import racing_gudetama from "../imgs/racing_gudetama.gif"
function Home() {
  return (
    <div className='p-5' style={{ display: 'flex', alignContent: 'center', justifyContent: 'center', margin: '0 auto', flexFlow: 'column', maxWidth: 800 }}>
      <h1>Schenectady Egg Sammy Central</h1>
      <Image src={racing_gudetama}></Image>
      <p>(translation: "Quickly! Let us find Egg Sammies to Review!")</p>
    </div>
  )
}

export default Home